## Variant Description

This variant is the same as the original design, however, it replaces the MCP73831T battery charging integrated circuit (IC) and the AP2112-3.3 linear voltage regulator IC, and the associated supporting circuits with the BQ25798 (battery charging IC) and TPS (buck/boost switching regulator).  This design provides through hole solder points for an external thermistor to monitor the battery temperature.
