This is a copy of the Adafruit Feather M4 Express PCB repository located on https://github.com at https://github.com/adafruit/Adafruit-Feather-M4-Express-PCB.

This repository is for experimenting with the power management and battery charging circuits of the original design.  The design variants will each utilize a battery charging integrated circuit capable of negotiating with a host system in order to autmatically set the charging current based on the host's port broadcasted capabilities.

The following is the original text of the README.md file located on github.

## Adafruit Feather M4 Express microcontroller board PCB

<a href="http://www.adafruit.com/products/3857"><img src="assets/image.jpg?raw=true" width="500px"><br/>
Click here to purchase one from the Adafruit shop</a>

PCB files for the Adafruit Feather M4 Express. PCB format is EagleCAD schematic and board layout

For more details, check out the product pages at
* https://www.adafruit.com/products/3857

### Description

It's what you've been waiting for, the Feather M4 Express featuring ATSAMD51. This Feather is fast like a swift, smart like an owl, strong like a ox-bird (it's half ox, half bird, OK?) This feather is powered by our new favorite chip, the ATSAMD51J19 -  with its 120MHz Cortex M4 with floating point support and 512KB Flash and 192KB RAM. Your code will zig and zag and zoom, and with a bunch of extra peripherals for support, this will for sure be your favorite new chipset.

And best of all, it's a Feather - so you know it will work with all our FeatherWings! What a great way to quickly get up and running.

The most exciting part of the Feather M4 is that while you can use it with the Arduino IDE - and it's bonkers fast when you do, we are shipping it with CircuitPython on board. When you plug it in, it will show up as a very small disk drive with main.py on it. Edit main.py with your favorite text editor to build your project using Python, the most popular programming language. No installs, IDE or compiler needed, so you can use it on any computer, even ChromeBooks or computers you can't install software on. When you're done, unplug the Feather and your code will go with you.

### License

Adafruit invests time and resources providing this open source design, please support Adafruit and open-source hardware by purchasing products from [Adafruit](https://www.adafruit.com)!

Designed by Limor Fried/Ladyada for Adafruit Industries.

Creative Commons Attribution/Share-Alike, all text above must be included in any redistribution. See license.txt for additional details.
