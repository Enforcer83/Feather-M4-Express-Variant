## Variant Description

This is the original design ported over to the KiCAD development environment.  The board file has minor changes to package placement due to using land patterns conforming to applicable IPC (https://www.ipc.org) standards.  All attempts have been made to locate componets as close to the their original positions as possible.  All copper placement has been added to match the original design as closely as possible.
